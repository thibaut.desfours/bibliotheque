package com.example.bibliotheque.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.bibliotheque.data.model.ObjectUtilisateur;

public class TableControllerUtilisateur extends DatabaseHandler{
    public TableControllerUtilisateur(Context context){
        super(context);
    }

    public ObjectUtilisateur readSingleRecord(String email) {
        ObjectUtilisateur user = null;
        String sql = "SELECT * FROM utilisateur WHERE email = '" + email + "';";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            String nom = cursor.getString(cursor.getColumnIndex("nom"));
            String prenom = cursor.getString(cursor.getColumnIndex("prenom"));
            String adresse = cursor.getString(cursor.getColumnIndex("adresse"));
            String telephone = cursor.getString(cursor.getColumnIndex("telephone"));
            String mdp = cursor.getString(cursor.getColumnIndex("mdp"));
            int role = cursor.getInt(cursor.getColumnIndex("role"));
            user = new ObjectUtilisateur(id, nom, prenom, email, adresse, telephone, role, mdp);
        }
        cursor.close();
        db.close();
        return user;
    }
}
