package com.example.bibliotheque.data.model;

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
public class ObjectUtilisateur {
    private int id;
    private String nom;
    private String prenom;
    private String email;
    private String adresse;
    private String telephone;
    private int role;
    private String mdp;

    public ObjectUtilisateur(int id, String nom, String prenom, String email, String adresse, String telephone, int role, String mdp) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.adresse = adresse;
        this.telephone = telephone;
        this.role = role;
        this.mdp = mdp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom){
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom){
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse){
        this.adresse = adresse;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone){
        this.telephone = telephone;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role){
        this.role = role;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp){
        this.mdp = mdp;
    }
}