package com.example.bibliotheque.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {
    private static int DATABASE_VERSION = 1;
    protected static final String DATABASE_NAME = "BibliothequeDatabase";

    public DatabaseHandler(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL("CREATE TABLE utilisateur ( id INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT, prenom TEXT, email TEXT, adresse TEXT, telephone TEXT, role INT, mdp TEXT);");
        db.execSQL("CREATE TABLE livre ( isbn TEXT PRIMARY KEY, designation TEXT, prix FLOAT);");
        db.execSQL("CREATE TABLE emprunt ( id INTEGER, isbn TEXT, dateD DATE, dateR DATE );");
        db.execSQL("INSERT INTO utilisateur values (1, 'Desfours', 'Thibaut', 'thibaut.desfours@viacesi.fr', '40 rue Ampère 61000 Alençon', '0652645897', 99, 'tibo99');");
        db.execSQL("INSERT INTO utilisateur values (2, 'Gémy', 'Dorian', 'dorian.gemy@viacesi.fr', '45 rue Ampère 61000 Alençon', '06526452561', 50, 'dobaut');");
        db.execSQL("INSERT INTO livre values (19265189421, 'L''intégrale de Brel', 15);");
        db.execSQL("INSERT INTO livre values (97856423612, 'Dobaut, l''autobiographie', 25);");
        db.execSQL("INSERT INTO emprunt values (1, 19265189421, '25-03-2021', null);");
        db.execSQL("INSERT INTO emprunt values (2, 97856423612, '01-04-2021', null);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        String sql = "DROP TABLE IF EXISTS utilisateur, livre, emprunt";
        db.execSQL(sql);
        onCreate(db);
    }
}
