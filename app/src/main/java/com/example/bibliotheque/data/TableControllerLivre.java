package com.example.bibliotheque.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.bibliotheque.data.model.ObjectLivre;
import com.example.bibliotheque.data.model.ObjectUtilisateur;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TableControllerLivre extends DatabaseHandler{
    public TableControllerLivre(Context context){
        super(context);
    }

    public boolean create(ObjectLivre objectLivre){
        ContentValues values = new ContentValues();
        values.put("isbn", objectLivre.getIbsn());
        values.put("designation", objectLivre.getDesignation());
        values.put("prix", objectLivre.getPrix());

        SQLiteDatabase db = this.getWritableDatabase();

        boolean createSuccessFul = db.insert("livre", null, values) > 0;
        db.close();
        return createSuccessFul;
    }

    public boolean createEmprunt(String isbn, int idUtilisateur){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String dateD = sdf.format(new Date());

        ContentValues values = new ContentValues();
        values.put("id", idUtilisateur);
        values.put("isbn", isbn);
        values.put("dateD", dateD);
        values.putNull("dateR");

        SQLiteDatabase db = this.getWritableDatabase();

        boolean createSuccessFul = db.insert("emprunt", null, values) > 0;
        db.close();

        return createSuccessFul;
    }

    public boolean estDejaEmprunte(String isbn) {
        String sql = "SELECT * FROM emprunt WHERE dateR IS NULL AND isbn = '" + isbn + "';";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        boolean isExist = false;
        if (cursor.moveToFirst()) {
            isExist = true;
        }
        cursor.close();
        db.close();
        return isExist;
    }

    public boolean updateEmprunt(String isbn, int idUtilisateur){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String dateR = sdf.format(new Date());

        ContentValues values = new ContentValues();
        values.put("dateR", dateR);
        String where = "isbn = ? AND id = ?";
        String[] whereArgs = { isbn, String.valueOf(idUtilisateur) };
        SQLiteDatabase db = this.getWritableDatabase();
        boolean updateSuccessful = db.update("emprunt", values, where, whereArgs) > 0;
        db.close();
        return updateSuccessful;
    }

    public boolean readSingleRecordEmprunt(String isbnLivre) {
        String sql = "SELECT * FROM livre WHERE isbn = '" + isbnLivre + "';";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        boolean isExist = false;
        if (cursor.moveToFirst()) {
            isExist = true;
        }
        cursor.close();
        db.close();
        return isExist;
    }

    public int count(){
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "SELECT * FROM livre";
        int recordCount = db.rawQuery(sql, null).getCount();
        db.close();

        return recordCount;
    }

    public List<ObjectLivre> read(){
        List<ObjectLivre> recordsList = new ArrayList<ObjectLivre>();
        String sql = "SELECT * FROM livre ORDER BY designation";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        if(cursor.moveToFirst()){
            do{
                String isbn = cursor.getString(cursor.getColumnIndex("isbn"));
                String designation = cursor.getString(cursor.getColumnIndex("designation"));
                float prix = cursor.getFloat(cursor.getColumnIndex("prix"));
                ObjectLivre objectLivre = new ObjectLivre(isbn, designation, prix);

                recordsList.add(objectLivre);
            }while(cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return recordsList;
    }

    public ObjectLivre readSingleRecord(String isbnLivre) {
        ObjectLivre objectLivre = null;
        String sql = "SELECT * FROM livre WHERE isbn = '" + isbnLivre + "';";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            String isbn = cursor.getString(0);
            String designation = cursor.getString(1);
            float prix = cursor.getFloat(2);
            objectLivre = new ObjectLivre(isbn, designation, prix);
        }
        cursor.close();
        db.close();
        return objectLivre;
    }

    public boolean update(ObjectLivre objectLivre) {
        ContentValues values = new ContentValues();
        values.put("designation", objectLivre.getDesignation());
        values.put("prix", objectLivre.getPrix());
        String where = "isbn = ?";
        String[] whereArgs = { objectLivre.getIbsn() };
        SQLiteDatabase db = this.getWritableDatabase();
        boolean updateSuccessful = db.update("livre", values, where, whereArgs) > 0;
        db.close();
        return updateSuccessful;
    }

    public boolean delete(String isbn) {
        boolean deleteSuccessful = false;
        SQLiteDatabase db = this.getWritableDatabase();
        deleteSuccessful = db.delete("livre", "isbn ='" + isbn + "'", null) > 0;
        db.close();
        return deleteSuccessful;
    }

    public ObjectUtilisateur estEmprunte(String isbnLivre){
        ObjectUtilisateur utilisateur = null;
        String sql = "SELECT * FROM utilisateur join emprunt on utilisateur.id = emprunt.id WHERE emprunt.isbn = '" + isbnLivre + "' and emprunt.dateR is null;";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            String nom = cursor.getString(cursor.getColumnIndex("nom"));
            String prenom = cursor.getString(cursor.getColumnIndex("prenom"));
            String email = cursor.getString(cursor.getColumnIndex("email"));
            String adresse = cursor.getString(cursor.getColumnIndex("adresse"));
            String telephone = cursor.getString(cursor.getColumnIndex("telephone"));
            String mdp = cursor.getString(cursor.getColumnIndex("mdp"));
            int role = cursor.getInt(cursor.getColumnIndex("role"));
            utilisateur = new ObjectUtilisateur(id, nom, prenom, email, adresse, telephone, role, mdp);
        }
        cursor.close();
        db.close();
        return utilisateur;
    }
}