package com.example.bibliotheque.data.model;

public class ObjectLivre {
    private String isbn;
    private String designation;
    private float prix;

    public ObjectLivre(String isbn, String designation, float prix){
        this.isbn = isbn;
        this.designation = designation;
        this.prix = prix;
    }

    public String getIbsn(){
        return isbn;
    }

    public void setIsbn(String isbn){
        this.isbn = isbn;
    }

    public String getDesignation(){
        return designation;
    }

    public void setDesignation(String designation){
        this.designation = designation;
    }

    public float getPrix(){
        return prix;
    }

    public void setPrix(float prix){
        this.prix = prix;
    }
}
