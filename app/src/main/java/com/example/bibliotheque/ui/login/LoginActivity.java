package com.example.bibliotheque.ui.login;

import android.app.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bibliotheque.MainActivity;
import com.example.bibliotheque.R;
import com.example.bibliotheque.data.TableControllerUtilisateur;
import com.example.bibliotheque.data.model.ObjectUtilisateur;


public class LoginActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        EditText editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        EditText editTextMdp = (EditText) findViewById(R.id.editTextMdp);
        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        Intent intent = new Intent(this, MainActivity.class);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ObjectUtilisateur utilisateur = checkLogin(editTextEmail.getText().toString(), editTextMdp.getText().toString());
                if(utilisateur != null){
                    intent.putExtra("user_email", utilisateur.getEmail());
                    intent.putExtra("user_role", utilisateur.getRole());
                    intent.putExtra("user_id", utilisateur.getId());
                    startActivity(intent);
                }
            }
        });
    }

    public ObjectUtilisateur checkLogin(String email, String mdp) {
        ObjectUtilisateur utilisateur = null;
        if (!email.equals("") && !mdp.equals("")) {
            utilisateur = new TableControllerUtilisateur(this).readSingleRecord(email);
            if(!utilisateur.getMdp().equals(mdp))
                utilisateur = null;
        }
        return utilisateur;
    }
}
