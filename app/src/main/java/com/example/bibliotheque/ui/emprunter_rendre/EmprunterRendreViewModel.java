package com.example.bibliotheque.ui.emprunter_rendre;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class EmprunterRendreViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public EmprunterRendreViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is home fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}