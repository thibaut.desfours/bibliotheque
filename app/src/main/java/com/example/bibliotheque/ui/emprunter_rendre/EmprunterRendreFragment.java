package com.example.bibliotheque.ui.emprunter_rendre;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.bibliotheque.R;
import com.example.bibliotheque.data.TableControllerLivre;
import com.example.bibliotheque.data.TableControllerUtilisateur;
import com.example.bibliotheque.data.model.ObjectUtilisateur;

public class EmprunterRendreFragment extends Fragment {

    private EmprunterRendreViewModel emprunterRendreViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        emprunterRendreViewModel =
                new ViewModelProvider(this).get(EmprunterRendreViewModel.class);
        View root = inflater.inflate(R.layout.fragment_emprunter_rendre, container, false);

        Button buttonEmprunt = (Button) root.findViewById(R.id.buttonEmprunter);
        buttonEmprunt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Context context = v.getRootView().getContext();
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View formElementsView = inflater.inflate(R.layout.fragment_emprunter_rendre, null, false);

                TextView tvISBN = (TextView) root.findViewById(R.id.editTextISBN);
                String valueISBN = tvISBN.getText().toString();

                Intent intent = getActivity().getIntent();
                if (intent != null) {
                    String userEmail = "";
                    if (intent.hasExtra("user_email")) {
                        userEmail = intent.getStringExtra("user_email");
                    }

                    final TableControllerUtilisateur tableControllerUtilisateur = new
                            TableControllerUtilisateur(formElementsView.getContext());
                    ObjectUtilisateur objectStudent = tableControllerUtilisateur.readSingleRecord(userEmail);

                    if (isIsbnExist(valueISBN)) {
                        if (!estDejaEmprunte(valueISBN)) {
                            if (emprunterLivre(valueISBN, objectStudent.getId())) {
                                new AlertDialog.Builder(getContext())
                                        .setMessage("Vous avez emprunté ce livre !")
                                        .show();
                            } else {
                                new AlertDialog.Builder(getContext())
                                        .setMessage("Une erreur est survenue ! Veuillez réassayer. Si le problème persiste, n'hésitez pas à contacter un administrateur.")
                                        .show();
                            }
                        } else {
                            new AlertDialog.Builder(getContext())
                                    .setMessage("Ce livre est déjà emprunté !")
                                    .show();
                        }
                    } else {
                        new AlertDialog.Builder(getContext())
                                .setMessage("Ce livre n'existe pas !")
                                .show();
                    }
                }
            }
        });

        Button buttonRendre = (Button) root.findViewById(R.id.buttonRendre);
        buttonRendre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Context context = v.getRootView().getContext();
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View formElementsView = inflater.inflate(R.layout.fragment_emprunter_rendre, null, false);

                TextView tvISBN = (TextView) root.findViewById(R.id.editTextISBN);
                String valueISBN = tvISBN.getText().toString();

                Intent intent = getActivity().getIntent();
                if (intent != null) {
                    String userEmail = "";
                    if (intent.hasExtra("user_email")) {
                        userEmail = intent.getStringExtra("user_email");
                    }

                    final TableControllerUtilisateur tableControllerUtilisateur = new
                            TableControllerUtilisateur(formElementsView.getContext());
                    ObjectUtilisateur objectStudent = tableControllerUtilisateur.readSingleRecord(userEmail);

                    if (isIsbnExist(valueISBN)) {
                        if (estDejaEmprunte(valueISBN)) {
                            if (rendreLivre(valueISBN, objectStudent.getId())) {
                                new AlertDialog.Builder(getContext())
                                        .setMessage("Vous avez rendu ce livre !")
                                        .show();
                            } else {
                                new AlertDialog.Builder(getContext())
                                        .setMessage("Une erreur est survenue ! Veuillez réassayer. Si le problème persiste, n'hésitez pas à contactez un administrateur.")
                                        .show();
                            }
                        } else {
                            new AlertDialog.Builder(getContext())
                                    .setMessage("Impossible de rendre le livre, car celui-ci n'a pas été emprunté !")
                                    .show();
                        }
                    } else {
                        new AlertDialog.Builder(getContext())
                                .setMessage("Ce livre n'existe pas !")
                                .show();
                    }
                }
            }
        });

        return root;
    }

    private boolean isIsbnExist(String isbn) {
        TableControllerLivre tcl = new TableControllerLivre(getContext());
        return tcl.readSingleRecordEmprunt(isbn);
    }

    private boolean estDejaEmprunte(String isbn) {
        TableControllerLivre tcl = new TableControllerLivre(getContext());
        return tcl.estDejaEmprunte(isbn);
    }

    private boolean emprunterLivre(String isbn, int idUtilisateur) {
        TableControllerLivre tcl = new TableControllerLivre(getContext());
        boolean createSuccessFul = tcl.createEmprunt(isbn, idUtilisateur);

        return createSuccessFul;
    }

    private boolean rendreLivre(String isbn, int idUtilisateur) {
        TableControllerLivre tcl = new TableControllerLivre(getContext());
        boolean createSuccessFul = tcl.updateEmprunt(isbn, idUtilisateur);

        return createSuccessFul;
    }
}