package com.example.bibliotheque.ui.admin;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.bibliotheque.R;
import com.example.bibliotheque.data.TableControllerLivre;
import com.example.bibliotheque.data.TableControllerUtilisateur;
import com.example.bibliotheque.data.model.ObjectLivre;
import com.example.bibliotheque.data.model.ObjectUtilisateur;

import java.util.List;

public class AdminFragment extends Fragment {

    private AdminViewModel adminViewModel;
    Context context;
    String idOpt;
    int role;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        adminViewModel = new ViewModelProvider(this).get(AdminViewModel.class);
        View root = inflater.inflate(R.layout.fragment_admin, container, false);
        Intent intent = getActivity().getIntent();
        Button btnAjout = (Button) root.findViewById(R.id.btnAjout);
        role = intent.getIntExtra("user_role", 0);
        if(role < 99){
            btnAjout.setVisibility(View.INVISIBLE);
        }
        readRecords(root);

        btnAjout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Context context = v.getRootView().getContext();
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View formElementsView = inflater.inflate(R.layout.livre_input_form, null, false);
                final EditText editTextIsbn = (EditText) formElementsView.findViewById(R.id.editTextIsbn);
                final EditText editTextDesignation = (EditText) formElementsView.findViewById(R.id.editTextDesignation);
                final EditText editTextPrix = (EditText) formElementsView.findViewById(R.id.editTextPrix);
                new AlertDialog.Builder(context)
                        .setView(formElementsView)
                        .setTitle("Creer un livre")
                        .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                String isbn = editTextIsbn.getText().toString();
                                String designation = editTextDesignation.getText().toString();
                                float prix = Float.parseFloat(editTextPrix.getText().toString());
                                ObjectLivre objectLivre = new ObjectLivre(isbn, designation, prix);
                                boolean createSuccessful = new TableControllerLivre(context).create(objectLivre);
                                if(createSuccessful){
                                    Toast.makeText(context, "Livre créé !", Toast.LENGTH_SHORT).show();
                                    readRecords(root);
                                }
                                else{
                                    Toast.makeText(context, "Impossible de créer le livre", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }).show();
            }
        });
        return root;
    }

    public void readRecords(View root){
        LinearLayout linearLayoutRecords = (LinearLayout) root.findViewById(R.id.linearLayoutRecords);
        context = root.getContext();
        linearLayoutRecords.removeAllViews();

        List<ObjectLivre> livres = new TableControllerLivre(root.getContext()).read();
        if (livres.size() > 0) {
            for (ObjectLivre obj : livres) {
                String isbn = obj.getIbsn();
                ObjectUtilisateur utilisateurEmprunt = new TableControllerLivre(root.getContext()).estEmprunte(isbn);
                String designation = obj.getDesignation();
                float prix = obj.getPrix();
                String textViewContents = isbn + " - " + designation + " : " + prix + " €";
                if(utilisateurEmprunt != null){
                        textViewContents += " - Emprunté";
                        if(role >= 99)
                            textViewContents += " par "+utilisateurEmprunt.getPrenom()+ " "+utilisateurEmprunt.getNom();
                }else{
                    textViewContents += " - Disponible";
                }
                TextView textViewLivreItem = new TextView(root.getContext());
                textViewLivreItem.setPadding(0, 10, 0, 10);
                textViewLivreItem.setText(textViewContents);
                textViewLivreItem.setTag(isbn);
                if(role >= 99) {
                    textViewLivreItem.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            context = v.getContext();
                            idOpt = v.getTag().toString();
                            final CharSequence[] items = {"Edit", "Delete"};
                            new AlertDialog.Builder(context).setTitle("Livre " + idOpt).setItems(items, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int item) {
                                    dialog.dismiss();
                                    if (item == 0) {
                                        editRecord(idOpt, root);
                                    } else if (item == 1) {
                                        boolean deleteSuccessful = new TableControllerLivre(context).delete(idOpt);
                                        if (deleteSuccessful) {
                                            Toast.makeText(context, "Livre supprimé.", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(context, "Impossible de supprimer le livre.", Toast.LENGTH_SHORT).show();
                                        }
                                        readRecords(root);
                                    }
                                }
                            }).show();
                            return false;
                        }
                    });
                }
                linearLayoutRecords.addView(textViewLivreItem);
            }
        }
        else{
            TextView locationItem = new TextView(context);
            locationItem.setPadding(8, 8, 8, 8);
            locationItem.setText("No records yet.");
            linearLayoutRecords.addView(locationItem);
        }
    }

    public void editRecord(String isbn, View root){
        final TableControllerLivre tableControllerLivre = new TableControllerLivre(context);
        ObjectLivre objectLivre = tableControllerLivre.readSingleRecord(isbn);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View formElementsView = inflater.inflate(R.layout.livre_input_form, null, false);
        final EditText editTextIsbn = (EditText) formElementsView.findViewById(R.id.editTextIsbn);
        final EditText editTextDesignation = (EditText) formElementsView.findViewById(R.id.editTextDesignation);
        final EditText editTextPrix = (EditText) formElementsView.findViewById(R.id.editTextPrix);
        editTextIsbn.setText(objectLivre.getIbsn());
        editTextDesignation.setText(objectLivre.getDesignation());
        editTextPrix.setText(String.valueOf(objectLivre.getPrix()));
        new AlertDialog.Builder(context)
                .setView(formElementsView)
                .setTitle("Edit Record")
                .setPositiveButton("Save Changes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        String isbn = editTextIsbn.getText().toString();
                        String designation = editTextDesignation.getText().toString();
                        float prix = Float.parseFloat(editTextPrix.getText().toString());
                        ObjectLivre objectLivre = new ObjectLivre(isbn, designation, prix);
                        boolean updateSuccessful = tableControllerLivre.update(objectLivre);
                        if(updateSuccessful){
                            Toast.makeText(context, "Student record was updated.", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(context, "Unable to update student record.", Toast.LENGTH_SHORT).show();
                        }
                        readRecords(root);
                    }
                }).show();
    }
}