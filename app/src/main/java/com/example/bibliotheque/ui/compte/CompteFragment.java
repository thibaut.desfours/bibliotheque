package com.example.bibliotheque.ui.compte;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.bibliotheque.MainActivity;
import com.example.bibliotheque.R;
import com.example.bibliotheque.data.TableControllerUtilisateur;
import com.example.bibliotheque.data.model.ObjectUtilisateur;
import com.example.bibliotheque.ui.login.LoginActivity;

public class CompteFragment extends Fragment {

    private CompteViewModel compteViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        compteViewModel =
                new ViewModelProvider(this).get(CompteViewModel.class);
        View root = inflater.inflate(R.layout.fragment_compte, container, false);
        Intent deco = new Intent(this.getContext(), LoginActivity.class);
        Button btnDeco = (Button) root.findViewById(R.id.button_deconnecter);
        btnDeco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(deco);
            }
        });

        Intent intent = getActivity().getIntent();
        if (intent != null) {
            String userEmail = "";
            if (intent.hasExtra("user_email")) {
                userEmail = intent.getStringExtra("user_email");
            }

            final TableControllerUtilisateur tableControllerUtilisateur = new
                    TableControllerUtilisateur(root.getContext());
            ObjectUtilisateur objectStudent = tableControllerUtilisateur.readSingleRecord(userEmail);

            TextView tvNom = (TextView) root.findViewById(R.id.data_nom);
            tvNom.setText(objectStudent.getNom());

            TextView tvPrenom = (TextView) root.findViewById(R.id.data_prenom);
            tvPrenom.setText(objectStudent.getPrenom());

            TextView tvEmail = (TextView) root.findViewById(R.id.data_email);
            tvEmail.setText(userEmail);

            TextView tvTelephone = (TextView) root.findViewById(R.id.data_telephone);
            tvTelephone.setText(objectStudent.getTelephone());

            TextView tvAdresse = (TextView) root.findViewById(R.id.data_adresse);
            tvAdresse.setText(objectStudent.getAdresse());
        }

        return root;
    }
}